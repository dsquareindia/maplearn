# -*- coding: utf-8 -*-
"""
Installateur de Mapping Learning
Created on Thu Oct 13 10:48:51 2016

@author: thomas_a
"""

import inspect
import os
import setuptools


def update_cfg_file(fichier, prefix):
    """ edite les fichiers de configuration pour les rendre utilisables sur le
    poste => PATCH à supprimer dés que possible"""

    with open(fichier) as __file:
        with open(fichier[:-4], 'w') as __file_out:
            for line in __file:
                line = line.format(path=prefix)
                __file_out.write(line)
    print("Configuration file %s updated" % fichier)

# application path
dir_app = os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe())))

try:
    reqs = open(os.path.join(dir_app, 'requirements.txt')).read()
except (IOError, OSError):
    print('Dépendances non remplies')

setuptools.setup(
    name='maplearn',
    version='0.2.0',
    description='Mapping Learning',
    url='https://bitbucket.org/thomas_a/maplearn/',
    author="Alban Thomas",
    license='LGPL',
    install_requires=reqs
)


def get_ori(dir):
    return [os.path.join(dir, i) for i in os.listdir(dir) if os.path.splitext(i)[-1] == '.ORI']

examples_path = os.path.join(dir_app, "examples")
for file_path in get_ori(examples_path) + get_ori(dir_app):
    update_cfg_file(file_path, dir_app)


# create tmp directory (for unit tests...)
if not os.path.exists(os.path.join(dir_app, 'tmp')):
    os.mkdir(os.path.join(dir_app, 'tmp'))
