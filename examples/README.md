# EXEMPLES : fichiers de configuration

Le dossier contient des fichiers de configuration qui peuvent vous aider à
paramétrer l'application pour vos propres besoins.

**ATTENTION**: les fichiers contiennent des chemins absolus qui nécessitent
d'être redéfinis avant de pouvoir les utiliser sur votre propre poste.